app.controller('ratePlanController',['$scope','$rootScope','$http','$location','$window',function($scope,$rootScope,$http,$location,$window){
	
	$scope.ratePlan = [];


	$scope.ratePlanVisible = false;
	$scope.isEdited = 'false';
	$scope.ratePlanForm = {};

	// import polices from previous view
	// $scope.policies = $rootScope.policies;

	// import fake policies data from file:
	$http.get('policies.json').then(function(res){
		$scope.policies = res.data;
	});

	// import fake room json data
	$http.get('rooms.json').then(function(res){
		$scope.roomsData = res.data;
	});

	var cancIndex, depoIndex;
	var roomTypes = [];
	var deposit, cancellation;
	var allRooms = true;
	

	$scope.roomRatePlan = function(per){
		$scope.ratePlanForm.per = per;
		if(per === "room"){
			$('#person').removeClass('clickedRooms');
			$('#room').addClass('clickedRooms');
		}
		else if(per === "person"){
			$('#room').removeClass('clickedRooms');
			$('#person').addClass('clickedRooms');
		}
	}

	$scope.assingRatePolicy = function(policy,index){
			if(policy === 'deposit'){
				$scope.ratePlanForm.deposit = $scope.policies.deposit[index];
				// assing index vaule to global var that allow to save deposit policy
				depoIndex = index;
				// change color of the clicked policy
				$('.depositPolicy').removeClass('clickedRooms');
				$('.depositPolicy').eq(index).addClass('clickedRooms');
				$('.dragAndDropBtn').eq(0).addClass('clickedRooms');
			}
			else if(policy === 'cancellation'){
				$scope.ratePlanForm.cancellation = $scope.policies.cancellation[index];
				// assing index vaule to global var that allow to save cancellation policy
				cancIndex = index;
				// change color of the clicked policy
				$('.cancellationPolicy').removeClass('clickedRooms');
				$('.cancellationPolicy').eq(index).addClass('clickedRooms');
				$('.dragAndDropBtn').eq(1).addClass('clickedRooms');
			}
			else if(policy === 'meal'){
				$('.mealPolicy').removeClass('clickedRooms');
				$('.mealPolicy').eq(index).addClass('clickedRooms');
				$('.dragAndDropBtn').eq(2).addClass('clickedRooms');
			}
			
	}

	$scope.chooseRoom = function(roomNum){
		
		var ifToRemove = false;

		if(roomNum == 'all'){
			roomTypes = [];
			if(allRooms)
			{
				$('#allRoomsBtn, .roomsBtn').addClass('clickedRooms');
				allRooms = !allRooms;
				for(var i in $scope.roomsData)
				{
					roomTypes.push(parseInt(i));
				}

			}
			else
			{
				$('#allRoomsBtn, .roomsBtn').removeClass('clickedRooms');
				allRooms = !allRooms;

			}
			
		}
		else{	
			if(!allRooms){allRooms = true};

			$('#allRoomsBtn').removeClass('clickedRooms');
			$('.roomsBtn').eq(roomNum).toggleClass('clickedRooms');

			for(var i = 0; i < roomTypes.length;i++){
				if(roomTypes[i] === roomNum){
					roomTypes.splice(i,1);
					// ifToRemove = true;
					return false;
				}
			}
			// if(!ifToRemove){roomTypes.push(roomNum)};
			roomTypes.push(roomNum);
			if(roomTypes.length == $scope.roomsData.length)
			{
				$('#allRoomsBtn, .roomsBtn').addClass('clickedRooms');
				allRooms = false;
			}
		}
	}

	$scope.saveRatePlan = function(){
		if(repeatedName($scope.ratePlanForm.name) && $scope.isEdited == 'false'){return alert('names cannot be repeated')};
		var oneRatePlan = {};

		if($scope.isEdited != 'false'){
			saveEdited();
			$scope.isEdited = 'false';
			return false;
		}
		oneRatePlan.name = $scope.ratePlanForm.name;
		oneRatePlan.per = $scope.ratePlanForm.per;
		oneRatePlan.meal = $scope.ratePlanForm.meal;
		if(depoIndex != null && cancIndex != null)
		{
		oneRatePlan.deposit = $scope.policies.deposit[depoIndex];
		oneRatePlan.cancellation = $scope.policies.cancellation[cancIndex];
		//assing additional variables in order to store cancellation/deposit object when edited
		deposit = oneRatePlan.deposit;
		cancellation = oneRatePlan.cancellation;
		}
		else{
			oneRatePlan.deposit = deposit;
			oneRatePlan.cancellation = cancellation;
		}

		//roomTypes assigned in chooseRoom fucntion defined whether all types of rooms
		// or just some of them will be saved
			oneRatePlan.rooms = [];

			for(var i = 0; i < roomTypes.length; i++){
				var n = roomTypes[i];
				oneRatePlan.rooms.push($scope.roomsData[n]);
			}
			
			roomTypes = [];
		
		// check whether plan in inherited from another one and assing those values
		if($scope.ratePlanForm.inherit == true && $scope.ratePlanForm.inheritAmount >= 0 && $scope.ratePlanForm.inheritType !== undefined)
		{
			oneRatePlan.inherit = {};
			oneRatePlan.inherit.from = $scope.ratePlanForm.inheritRate.name;
			oneRatePlan.inherit.amount = $scope.ratePlanForm.inheritAmount;
			oneRatePlan.inherit.type = $scope.ratePlanForm.inheritType;
		}
	
		
		//if inherited change order of array to display inherited rate plan under its parent 
		if(oneRatePlan.inherit)
		{	
			inheritanceOrder(oneRatePlan);
		}
		else
		{
			$scope.ratePlan.push(oneRatePlan);
		}
		
		$scope.cleanForm();
	}

	$scope.cleanForm = function(){
		cancIndex = depoIndex = null;
		$scope.ratePlanForm.cancellation = $scope.ratePlanForm.deposit = $scope.ratePlanForm.meal = null;

	 	$scope.ratePlanForm.name = null
		$scope.ratePlanForm.per = null;
		$scope.ratePlanForm.rooms = null;
		//reset all marked colors
		$('#person, #room, .mealPolicy, .depositPolicy, .cancellationPolicy').removeClass('clickedRooms');
		$('.dragAndDropBtn').removeClass('clickedRooms');

		$scope.ratePlanVisible = false;
		$scope.ratePlanForm.yes = false;
		$scope.ratePlanForm.inherit = false;
		$scope.isEdited = 'false';
		$('#allRoomsBtn').removeClass('clickedRooms');
		$('.roomsBtn').removeClass('clickedRooms');
		$('#person').removeClass('clickedRooms');
		$('#room').removeClass('clickedRooms');

		$window.scrollTo(0, 0);

	}

	$scope.findRatePlan = function(rateIndex){
		$scope.ratePlanToDelete = rateIndex;
	}

	$scope.deleteRatePlan = function(isEdited){
		if(isEdited != false){
			$scope.ratePlan.splice(isEdited,1);
			return true;
		}
		$scope.ratePlan.splice($scope.ratePlanToDelete,1);
		$scope.ratePlanToDelete = null;
	}

	$scope.editRatePlan = function(rateIndex){

		$scope.isEdited = rateIndex;

		$scope.ratePlanVisible = true;
		$scope.ratePlanForm.firstName = $scope.ratePlan[rateIndex].name;
		$scope.ratePlanForm.name = $scope.ratePlan[rateIndex].name;
		$scope.ratePlanForm.per = $scope.ratePlan[rateIndex].per;
		// HERE
		$scope.ratePlanForm.deposit = $scope.ratePlan[rateIndex].deposit;
		$scope.ratePlanForm.cancellation = $scope.ratePlan[rateIndex].cancellation;
		$scope.ratePlanForm.meal = $scope.ratePlan[rateIndex].meal;
		
		//assing policies buttons to edit panel
		$('.depositPolicy:contains('+$scope.ratePlanForm.deposit.name+')').addClass('clickedRooms');
		$('.mealPolicy:contains('+$scope.ratePlanForm.meal+')').addClass('clickedRooms');
		$('.cancellationPolicy:contains('+$scope.ratePlanForm.cancellation.name+')').addClass('clickedRooms');

		$('.dragAndDropBtn').addClass('clickedRooms');

		// assing buttons to payment per room or per person panel
		if($scope.ratePlan[rateIndex].per == 'room')
		{
			$('#person').removeClass('clickedRooms');
			$('#room').addClass('clickedRooms');
		}
		else
		{
			$('#room').removeClass('clickedRooms');
			$('#person').addClass('clickedRooms');
		};

		
		//assign rooms buttons
			for(var i  = 0; i < Object.keys($scope.ratePlan[rateIndex].rooms).length; i++){
				var name = $scope.ratePlan[rateIndex].rooms[i].name;
				$('.roomsBtn:contains('+name+')').addClass('clickedRooms');

			}

			if($scope.ratePlan[rateIndex].rooms.length == $scope.roomsData.length){
				$('#allRoomsBtn').addClass('clickedRooms');
			}
			for(var i in $scope.roomsData){
				for(var n in $scope.ratePlan[rateIndex].rooms){
					if($scope.roomsData[i].name == $scope.ratePlan[rateIndex].rooms[n].name)
					{
						roomTypes.push(parseInt(i));
					}
				}
			}
			

	}

	

	$scope.inherit = function(inheritRate){
		$scope.ratePlanForm.inherit = true;
	}

	//send user to rates view
	$scope.goRates = function(){
		console.log(JSON.stringify($scope.ratePlan));
		$location.path('/rates');
	}

	function saveEdited(){
		for(var i in $scope.ratePlan)
		{
			if($scope.ratePlanForm.firstName == $scope.ratePlan[i].name)
			{	
			
				$scope.ratePlan[i].name = $scope.ratePlanForm.name;
				$scope.ratePlan[i].per = $scope.ratePlanForm.per;
				$scope.ratePlan[i].deposit = $scope.ratePlanForm.deposit;
				$scope.ratePlan[i].cancellation = $scope.ratePlanForm.cancellation;
				$scope.ratePlan[i].meal = $scope.ratePlanForm.meal;
				$scope.ratePlan[i].rooms = [];
				
				//assing rooms to edited rate plan
				for(var n in roomTypes){
					var x = roomTypes[n];
					$scope.ratePlan[i].rooms.push($scope.roomsData[x]);
				}
				roomTypes = [];
			
				$scope.cleanForm();
				return true;
			}
		}
		
	}

	function inheritanceOrder(inheritedRate){
		if($scope.ratePlan.length == 1){
			$scope.ratePlan.push(inheritedRate);
			return true;
		}
		
		for(var i = 0; i < $scope.ratePlan.length; i++)
		{
			if($scope.ratePlan[i].name === inheritedRate.inherit.from)
			{	
				$scope.ratePlan.splice(i+1,0,inheritedRate);
				return true;
			}	
		}
	}

	function repeatedName(name){
		for(var i = 0; i < $scope.ratePlan.length;i++){
			if($scope.ratePlan[i].name === name){
				return true;
			}
		}
		return false;
	}

}]);