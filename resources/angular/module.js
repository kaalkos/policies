var app = angular.module('onboarding',['ngRoute','ngSanitize','ngStorage']);

app.config(function($routeProvider,$locationProvider){
	$routeProvider
	.when('/policies',{
		templateUrl: 'angular/policies/policies.html',
		controller: 'policiesController'
	})
	.when('/ratePlan',{
		templateUrl: 'angular/ratePlan/ratePlan.html',
		controller: 'ratePlanController'
	})
	.when('/rates',{
		templateUrl: 'angular/rates/rates.html',
		controller: 'ratesController'
	})

	 .otherwise({redirectTo:'/'});
	// $locationProvider.html5Mode({
 // 	enabled: true,
 //  	requireBase: false
	// });
	
});