var express = require('express');
var app = express();
var PORT = process.env.PORT || 8080;


app.use(express.static('resources'));
app.get('/',function(req,res){
	res.sendFile(__dirname + '/' + 'index.html');
});

app.listen(PORT,function(err){
	if(err){return console.log(err)};
	console.log('server is running on port: ' + PORT);
});
